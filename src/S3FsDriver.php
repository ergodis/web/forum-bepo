<?php

namespace Ergodis\Flarum;

use Flarum\Filesystem\DriverInterface;
use Flarum\Settings\SettingsRepositoryInterface;
use Flarum\Foundation\Config;
use Illuminate\Contracts\Filesystem\Cloud;
use Illuminate\Filesystem\FilesystemAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Aws\S3\S3Client;

class S3FsDriver implements DriverInterface
{
    public function build(string $diskName, SettingsRepositoryInterface $settings, Config $config, array $localConfig): Cloud
    {
        $client = new S3Client($config['s3']);
        $adapter = new AwsS3Adapter($client, $config['buckets'][$diskName]);
        $filesystem = new Filesystem($adapter);
        return new FilesystemAdapter($filesystem);
    }
}
