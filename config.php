<?php return [
    'debug' => false,
    'database' => [
        'driver' => 'mysql',
        'host' => getenv('MYSQL_ADDON_HOST'),
        'port' => getenv('MYSQL_ADDON_PORT'),
        'database' => getenv('MYSQL_ADDON_DB'),
        'username' => getenv('MYSQL_ADDON_USER'),
        'password' => getenv('MYSQL_ADDON_PASSWORD'),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => 'flarum_',
        'strict' => false,
        'engine' => 'InnoDB',
        'prefix_indexes' => true,
    ],
    's3' => [
        'endpoint' => 'https://' . getenv('CELLAR_ADDON_HOST'),
        'credentials' => [
            'key' => getenv('CELLAR_ADDON_KEY_ID'),
            'secret' => getenv('CELLAR_ADDON_KEY_SECRET'),
        ],
        'region' => 'default',
        'use_path_style_endpoint' => true,
        'version' => 'latest',
    ],
    'buckets' => [
        'flarum-assets' => getenv('FLARUM_ASSETS_BUCKET'),
        'flarum-avatars' => getenv('FLARUM_AVATARS_BUCKET'),
        'flarum-uploads' => getenv('FLARUM_UPLOADS_BUCKET'),
    ],
    'disk_driver' => [
        'flarum-assets' => 's3',
        'flarum-avatars' => 's3',
        'flarum-uploads' => 's3',
    ],
    'url' => 'https://' . ( ( isset($_SERVER) && array_key_exists('SERVER_NAME', $_SERVER) && str_ends_with($_SERVER['SERVER_NAME'], 'cleverapps.io') ) ? $_SERVER['SERVER_NAME'] : 'forum.bepo.fr' ),
    'paths' => [
        'api' => 'api',
        'admin' => 'admin',
    ],
    'headers' => [
        'poweredByHeader' => true,
        'referrerPolicy' => 'same-origin',
    ],
];
