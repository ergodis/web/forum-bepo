<?php

/*
 * This file is part of Flarum.
 *
 * For detailed copyright and license information, please view the
 * LICENSE file that was distributed with this source code.
 */

use Flarum\Extend;

return [
    (new Extend\Filesystem)->driver('s3', Ergodis\Flarum\S3FsDriver::class),
    new Blomstra\Redis\Extend\Redis([
        'host' => getenv('REDIS_HOST'),
        'port' => getenv('REDIS_PORT'),
        'password' => getenv('REDIS_PASSWORD'),
        'database' => 0,
    ]),
    (new Extend\Console)->command(Blomstra\CacheAssets\Command\CacheAssetsCommand::class),
];
