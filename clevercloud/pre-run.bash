#!/bin/bash -e

patch -p1 -i clevercloud/custom.patch

php flarum migrate
php flarum cache:clear
php flarum assets:publish
php flarum cache:assets

php flarum extension:enable migratetoflarum-old-passwords
